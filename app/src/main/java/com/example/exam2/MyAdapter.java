package com.example.exam2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Guide> guides = new ArrayList<>();

    public MyAdapter(Context context, ArrayList<Guide> guides) {
        this.context = context;
        this.guides = guides;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.grid_data, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.textGuide.setText(guides.get(position).getGuidName());
        holder.textCity.setText(guides.get(position).getGuidCity());
        holder.textPrice.setText(guides.get(position).getGuidPrice());
        holder.imgGrid.setImageResource(guides.get(position).getGuidImage());
        holder.layoutData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailFragment detailFragment = DetailFragment.newInstance(
                    guides.get(holder.getAdapterPosition()).getGuidName(),
                    guides.get(holder.getAdapterPosition()).getGuidCity(),
                    guides.get(holder.getAdapterPosition()).getGuidPrice(),
                    guides.get(holder.getAdapterPosition()).getGuidDesc(),
                    guides.get(holder.getAdapterPosition()).getGuidImage(),
                    guides.get(holder.getAdapterPosition()).getGuideImages()
                );
                FragmentManager fragmentManager=((FragmentActivity)context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_frame, detailFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return guides.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgGrid;
        private TextView textGuide;
        private TextView textCity;
        private TextView textPrice;

        private ConstraintLayout layoutData;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imgGrid = itemView.findViewById(R.id.imgGrid);
            textGuide = itemView.findViewById(R.id.textGuide);
            textCity = itemView.findViewById(R.id.textCity);
            textPrice = itemView.findViewById(R.id.textPrice);
            layoutData = itemView.findViewById(R.id.layoutData);
        }
    }
}
