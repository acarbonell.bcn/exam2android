package com.example.exam2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter2 extends RecyclerView.Adapter<MyAdapter2.MyViewHolder> {

    private Context context;
    private ArrayList<Integer> pics = new ArrayList<>();

    public MyAdapter2(Context context, ArrayList<Integer> pics) {
        this.context = context;
        this.pics = pics;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.layout_images, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.imgs.setImageResource(pics.get(position));
    }

    @Override
    public int getItemCount() {
        return pics.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgs;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imgs = itemView.findViewById(R.id.imagesDet);
        }
    }
}
