package com.example.exam2;

import java.util.ArrayList;

public class Guide {
    private String guidName;
    private String guidCity;
    private String guidPrice;
    private int guidImage;
    private String guidDesc;
    private ArrayList<Integer> guideImages;

    public Guide(String guidName, String guidCity, String guidPrice, int guidImage, String guidDesc, ArrayList<Integer> guideImages) {
        this.guidName = guidName;
        this.guidCity = guidCity;
        this.guidPrice = guidPrice;
        this.guidImage = guidImage;
        this.guidDesc = guidDesc;
        this.guideImages = guideImages;
    }

    public String getGuidName() {
        return guidName;
    }

    public void setGuidName(String guidName) {
        this.guidName = guidName;
    }

    public String getGuidCity() {
        return guidCity;
    }

    public void setGuidCity(String guidCity) {
        this.guidCity = guidCity;
    }

    public String getGuidPrice() {
        return guidPrice;
    }

    public void setGuidPrice(String guidPrice) {
        this.guidPrice = guidPrice;
    }

    public int getGuidImage() {
        return guidImage;
    }

    public void setGuidImage(int guidImage) {
        this.guidImage = guidImage;
    }

    public String getGuidDesc() {
        return guidDesc;
    }

    public void setGuidDesc(String guidDesc) {
        this.guidDesc = guidDesc;
    }

    public ArrayList<Integer> getGuideImages() {
        return guideImages;
    }

    public void setGuideImages(ArrayList<Integer> guideImages) {
        this.guideImages = guideImages;
    }
}
