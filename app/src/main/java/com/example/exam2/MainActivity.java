package com.example.exam2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNav;
    private FrameLayout mainFrame;
    private Toolbar toolbar;

    private GuideFragment guideFragment;
    private SitesFragment sitesFragment;
    private RestaurantFragment restaurantFragment;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.toolbar_nav, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_email:
                Toast.makeText(this, "Email", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.nav_language:
                Toast.makeText(this, "Language", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.nav_settings:
                Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNav = findViewById(R.id.bottom_nav);
        mainFrame = findViewById(R.id.main_frame);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        guideFragment = new GuideFragment();
        sitesFragment = new SitesFragment();
        restaurantFragment = new RestaurantFragment();

        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_restaurant:
                        setFragment(restaurantFragment);
                        bottomNav.setItemBackgroundResource(R.color.blue);
                        toolbar.setBackgroundResource(R.color.blue);
                        return true;
                    case R.id.nav_sites:
                        setFragment(sitesFragment);
                        bottomNav.setItemBackgroundResource(R.color.green);
                        toolbar.setBackgroundResource(R.color.green);
                        return true;
                    case R.id.nav_guide:
                        setFragment(guideFragment);
                        bottomNav.setItemBackgroundResource(R.color.red);
                        toolbar.setBackgroundResource(R.color.red);
                        return true;
                    default:
                        return false;

                }
            }
        });
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }
}