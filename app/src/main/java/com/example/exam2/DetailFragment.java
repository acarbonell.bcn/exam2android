package com.example.exam2;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    private static final String ARG_PARAM6 = "param6";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;
    private int mParam5;
    private ArrayList<Integer> mParam6;

    public DetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @param guidName
     * @param guidCity
     * @param guidPrice
     * @param guidDesc
     * @param guidImage
     * @param guideImages
     * @return A new instance of fragment DetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailFragment newInstance(String guidName, String guidCity, String guidPrice, String guidDesc, int guidImage, ArrayList<Integer> guideImages) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, guidName);
        args.putString(ARG_PARAM2, guidCity);
        args.putString(ARG_PARAM3, guidPrice);
        args.putString(ARG_PARAM4, guidDesc);
        args.putInt(ARG_PARAM5, guidImage);
        args.putIntegerArrayList(ARG_PARAM6, guideImages);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        ImageView detImg = view.findViewById(R.id.detImg);
        TextView detTitle = view.findViewById(R.id.detTitle);
        TextView detCity = view.findViewById(R.id.detCity);
        TextView detPrice = view.findViewById(R.id.detPrice);
        TextView detDesc = view.findViewById(R.id.detDesc);
        RecyclerView detArrayImgs = view.findViewById(R.id.detArrayImgs);



        if (getArguments() != null) {
            mParam1 =getArguments().getString(ARG_PARAM1);
            mParam2 =getArguments().getString(ARG_PARAM2);
            mParam3 =getArguments().getString(ARG_PARAM3);
            mParam4 =getArguments().getString(ARG_PARAM4);
            mParam5 =getArguments().getInt(ARG_PARAM5, 0);
            mParam6 =getArguments().getIntegerArrayList(ARG_PARAM6);
            detTitle.setText(mParam1);
            detCity.setText(mParam2);
            detPrice.setText(mParam3);
            detDesc.setText(mParam4);
            detImg.setImageResource(mParam5);
            MyAdapter2 myAdapter2 = new MyAdapter2(view.getContext(), mParam6);
            detArrayImgs.setAdapter(myAdapter2);
            detArrayImgs.setLayoutManager(new LinearLayoutManager(view.getContext()));

        }

        return view;
    }
}